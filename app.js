function xo(str) {
  // you can only write your code here!
  let strArray = str.toLowerCase().split('');

  const X = strArray.filter((item) => item === 'x');
  const O = strArray.filter((item) => item === 'o');

  if (O.length == X.length) {
    return true;
  } else {
    return false;
  }
}

// TEST CASES
console.log(xo('xoxoxo')); // true
console.log(xo('oxooxo')); // false
console.log(xo('oxo')); // false
console.log(xo('xxxooo')); // true
console.log(xo('xoxooxxo')); // true